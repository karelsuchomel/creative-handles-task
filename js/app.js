$(document).ready(function () {
    var hamburger = $('.hamburger');
    var nav = $('.navigation');
    var dropdownArrow = $('.dropdown-arrow');
    var dropdownMenu = $('.dropdown');
    var navlinks = $('.navigation li a');
    hamburger.on('click', function () {
        hamburger.toggleClass('is-active');
        nav.toggleClass('nav-active');
    });

    dropdownArrow.on('click', function () {
        dropdownMenu.toggleClass('opened');
    });

    navlinks.on('click', function () {
        $('li a').removeClass('active');
        $(this).addClass('active');
    });
});
